﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Hw19
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int flag = 0;
            string line;
            string name = User.Text;
            string password = Password.Text;
            name +=',' + password;
            try
            {
                System.IO.StreamReader file = new System.IO.StreamReader(@"C:\CppWork_Matan\Hw19\Hw19\Hw19\Users.txt");
                while ((line = file.ReadLine()) != null)
                {
                    if (line == name)
                    {
                        flag = 1;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                return;
            }
            if(flag == 0)
                MessageBox.Show("Wrong name or password");
            else
            {
                Form1 wnd = new Form1(User.Text);
                wnd.ShowDialog();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
