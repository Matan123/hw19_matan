﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Hw19
{
    public partial class Form1 : Form
    {
        List<string> Dates = new List<string>();
        public string FileName;

        public Form1(string FileName)
        {
            InitializeComponent();
            OpenFile(FileName);
        }

        public void OpenFile(string FileName)
        {
            string path = @"C:\CppWork_Matan\Hw19\Hw19\Hw19\" + FileName + "BD.txt";
            string line;
            try
            {
                System.IO.StreamReader file = new System.IO.StreamReader(path);
                while ((line = file.ReadLine()) != null)
                {
                    Dates.Add(line);
                }
            }
            catch (Exception ex)
            {
                File.WriteAllText(path," ");
                return;
            }
        }

        private void monthCalendar1_DateSelected(object sender, DateRangeEventArgs e)
        {
            int space1 = e.Start.ToString().IndexOf(' ');
            foreach (string date in Dates)
            {
                string a = e.Start.ToString().Substring(0, space1 - 5);
                int nameInd = date.IndexOf(',');
                if (date.Substring(nameInd).Contains(e.Start.ToString().Substring(0, space1 - 5)))
                {
                    Label.Text = "Happy birthday! " + date.Substring(0, nameInd);
                    return;
                }
            }
            Label.Text = "בתאריך הנבחר – אף אחד לא חוגג יום הולדת";
        }

    }
}
